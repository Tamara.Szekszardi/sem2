# Mitt program

Dokumentasjon til semesteroppgave 2: 

This game is based on Tron Light Cycles - 2 players

The game is played on a grid-like board. As the players move, a trail is left behind them, which forms a wall. The goal is to strategically create walls that will trap other players, while avoiding getting trapped yourself.

If a player crashes into a wall, they are out of the game. The last player standing is the winner.

Players move by using W-A-S-D and the arrow keys.

Try to trap each other by creating walls, while avoiding getting trapped yourself in order to win.

Good luck