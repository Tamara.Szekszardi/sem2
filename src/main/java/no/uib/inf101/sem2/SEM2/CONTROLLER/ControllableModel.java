package no.uib.inf101.sem2.SEM2.CONTROLLER;

import no.uib.inf101.sem2.SEM2.MODEL.GameState;
import no.uib.inf101.sem2.SEM2.MODEL.Player;

public interface ControllableModel {
    /**
     * 
     * @return Player 1
     */
    public Player getPlayer1();

    /**
     * 
     * @return Player 2
     */
    public Player getPlayer2();

    /**
     * 
     * @return current Gamestate
     */
    public GameState getGameState();

    /**
     * 
     * @param gs
     * @return new GameState
     */
    public GameState setGameState(GameState gs);

    /**
     * sets gamestate back to start
     * @return Gamestate.WELCOME
     */
    public GameState resetGamestate();

    /** 
     * gets called every tiime the clock ticks
    */
    public void clockTick();

    /**
     * millisecounds between clockticks
     * @return
     */
    public int getMilSec();
}
