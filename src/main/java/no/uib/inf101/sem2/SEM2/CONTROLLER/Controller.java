package no.uib.inf101.sem2.SEM2.CONTROLLER;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;

import javax.swing.Timer;

import no.uib.inf101.sem2.SEM2.MODEL.Direction;
import no.uib.inf101.sem2.SEM2.MODEL.GameState;
import no.uib.inf101.sem2.SEM2.MODEL.Player;
import no.uib.inf101.sem2.SEM2.VIEW.SemView;

public class Controller implements KeyListener {
    ControllableModel model;
    SemView view;
    ActionEvent klokkeslag;
    Timer timer;

    public Controller(ControllableModel model, SemView view) {
        this.model = model;
        this.view = view;
        this.timer = new Timer(model.getMilSec(), this::clockTick);
        
        view.addKeyListener(this);
        view.setFocusable(true);
        timer.start();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        Player player1 = model.getPlayer1();
        Player player2 = model.getPlayer2();

        if (model.getGameState() == GameState.WELCOME) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                model.setGameState(GameState.ACTIVE);
            }
        }
        if (model.getGameState() == GameState.GAME_OVER) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                model.resetGamestate();
            }
        }
        if (model.getGameState() == GameState.ACTIVE) {
            if (e.getKeyCode() == KeyEvent.VK_D) {
                // Left arrow was pressed
                player1.setDirection(Direction.EAST);
                
            } else if (e.getKeyCode() == KeyEvent.VK_A) {
                // Right arrow was pressed
                player1.setDirection(Direction.WEST);
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                // Down arrow was pressed
                player1.setDirection(Direction.SOUTH);
            } else if (e.getKeyCode() == KeyEvent.VK_W) {
                // Up arrow was pressed: rotate
                player1.setDirection(Direction.NORTH);
            } 
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                // Left arrow was pressed
                //System.out.println("kake");
                player2.setDirection(Direction.WEST);
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                // Right arrow was pressed
                player2.setDirection(Direction.EAST);
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                // Down arrow was pressed
                player2.setDirection(Direction.SOUTH);
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                // Up arrow was pressed: rotate
                player2.setDirection(Direction.NORTH);   
            }
            
        }view.repaint();
    }

    @Override
    public void keyReleased(KeyEvent arg0) {}

    @Override
    public void keyTyped(KeyEvent arg0) {}
    
    
    private void clockTick(ActionEvent ae) {
        if (model.getGameState() == GameState.ACTIVE) {
            model.clockTick();
            getRightDelay(); //
            view.repaint();
        }
    }

    /**
     * henter ut riktig delay fra modellen
     */
    private void getRightDelay() {
        int rightDelay = model.getMilSec();
        timer.setDelay(rightDelay);
        timer.setInitialDelay(rightDelay);
    }
}
