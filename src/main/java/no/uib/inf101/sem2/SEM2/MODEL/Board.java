package no.uib.inf101.sem2.SEM2.MODEL;

import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.CellPosition;

public class Board extends Grid<Character> {

    public Board(int rows, int cols) {
        super(rows, cols, '-');
    }

    /**
     * @return a string representation of the Board
     */
    public String stringBoard() {
        String StringBoard = "";
        for (int r = 0; r < this.rows(); r++) {
            for (int c = 0; c < this.cols(); c++) {
                StringBoard += this.get(new CellPosition(r, c));
            }
            if (r < this.rows() - 1) {
                StringBoard += ('\n');
            }
        }
        return StringBoard;
    }

    /**
     * sets the lines behind the players
     * @param p
     */
    public void setLineTiles(Player p) {
        CellPosition position = p.previousCP();
        char charValue = p.getCharColor();
        this.set(position, charValue);
    }


    /**
     * cleans the board
     */
    public void removeAll() {
        for (int r = 0; r < this.rows(); r++) {
            for (int c = 0; c < this.cols(); c++) {
                this.set(new CellPosition(r, c), '-');
            }
        }
    }

}
