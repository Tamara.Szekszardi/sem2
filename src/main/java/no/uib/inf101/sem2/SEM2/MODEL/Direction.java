package no.uib.inf101.sem2.SEM2.MODEL;

public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;
}
