package no.uib.inf101.sem2.SEM2.MODEL;

public enum GameState {
    WELCOME,
    ACTIVE,
    GAME_OVER;
}
