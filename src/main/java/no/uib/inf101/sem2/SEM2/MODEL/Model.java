package no.uib.inf101.sem2.SEM2.MODEL;

import java.util.Random;

import no.uib.inf101.sem2.SEM2.CONTROLLER.ControllableModel;
import no.uib.inf101.sem2.SEM2.VIEW.ViewableModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class Model implements ViewableModel, ControllableModel {
    private Board board;
    private Player player1;
    private Player player2;
    private GameState gState;
    private char player1char;
    private char player2char;
    private int score1;
    private int score2;
    private Player winner;

    public Model(Board board) {
        this.board = board;
        this.score1 = 0;
        this.score2 = 0;
        setPlayersColor();
        startGame();
    }

    /**
     * sets player colors
     * makes sure that two players will not have the same colors
     */
    private void setPlayersColor() {
        this.player1char = randomColor();
        this.player2char = randomColor();
        while (this.player1char == this.player2char) {
            player2char = randomColor();
        }
    }

    private void startGame() {
        this.player1 = new Player(player1char, randomCP(board), Direction.NORTH);
        CellPosition p2Pos = randomCP(board);
        while (!notTheSamePlayerPos(p2Pos)) {
            p2Pos = this.player1.getCellPosition();
        }
        this.player2 = new Player(player2char, randomCP(board), Direction.NORTH);
        gState = GameState.WELCOME;
    }

    @Override
    public GridDimension getDimension() {
        return board;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return board;
    }

    @Override
    public Player getPlayer1() {
        return this.player1;
    }

    @Override
    public Player getPlayer2() {
        return this.player2;
    }

    private char randomColor() {
        String str = "roygcbmp";
        Random random = new Random();
        int index = random.nextInt(str.length());
        return str.charAt(index);
    }

    /**
     * makes sure that two players will not have the same starting position
     * @param pos
     * @return
     */
    private boolean notTheSamePlayerPos(CellPosition pos) {
        if (pos != this.player1.getCellPosition()) {
            return true;
        }
        return false;
    }

    /**
     * chooses a rantom cellPosition 
     * used at gamestart
     * @param board
     * @return
     */
    private CellPosition randomCP(Board board) {
        Random rand = new Random();

        int minRow = board.rows() /5;
        int maxRow = board.rows() - minRow;
        int randomRow = rand.nextInt(minRow, maxRow);


        int minCol = board.cols() /5;
        int maxCol = board.cols() - minCol;
        int randomCol = rand.nextInt(minCol, maxCol);

        return new CellPosition(randomRow, randomCol);
    }

    /**
     * moves player - called in clockTic()
     * @param p
     * @param d
     * @return
     */
    private boolean movePlayer(Player p, Direction d) {
        Player candidate = p.whereToStep(d);
        if (legitPos(p)) {
            board.setLineTiles(p);
            p = candidate;
            return true;
        }
        return false;
    }

    /**
     * checks for Legal Position
     * @param p
     * @return
     */
    private boolean legitPos(Player p) {
        CellPosition playerPos = p.getCellPosition();
        if (!board.positionIsOnGrid(playerPos)) {
            return false;
        } else if (board.get(playerPos).charValue() != '-') {
            return false;
        } return true;
    }

    @Override
    public GameState getGameState() {
        return this.gState;
    }

    @Override
    public GameState setGameState(GameState gs) {
        this.gState = gs;
        return this.gState;
    }

    @Override
    public GameState resetGamestate() {
        board.removeAll();
        startGame();
        this.gState = GameState.WELCOME;
        return this.gState;
    }

    @Override
    public void clockTick() { 
        Boolean if1 = false; 
        Boolean if2 = false; 
        if (!movePlayer(player1, player1.getDirection())) {
            this.winner = player2;
            if1 = true;
            gState = setGameState(GameState.GAME_OVER);
        }
        if (!movePlayer(player2, player2.getDirection())) {
            this.winner = player1;
            if2 = true;
            gState = setGameState(GameState.GAME_OVER);
        } 
        if ((if1 && if2) || (player1.getCellPosition().equals(player2.getCellPosition()))) {
            this.winner = new Player('w', null, null);
            gState = setGameState(GameState.GAME_OVER);
        } else if (this.winner == player2 || this.winner == player1) {
            countScore();
        }
    }

    @Override
    public int getMilSec() {
        return 700; //sett inn vanskelighetsgrad
    }

    @Override
    public int getscoreP1() {
        return this.score1;
    }

    @Override
    public int getscoreP2() {
        return this.score2;
    }

    private void countScore() {
        if (this.winner == player1) {
            this.score1 ++;
        } else if (this.winner == player2) {
            this.score2 ++;
        }
    }

    @Override
    public String getWinnerOnView() {
        String w = "";
        if (this.winner == player1) {
            w = "PLAYER 1 WON";
        } else if (this.winner == player2) {
            w = "PLAYER 2 WON";
        } else {
            w = "LOOSERS";
        }
        return w;
    }

    @Override
    public Player getWinner() {
        return this.winner;
    }
}
