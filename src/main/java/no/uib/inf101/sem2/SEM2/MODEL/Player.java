package no.uib.inf101.sem2.SEM2.MODEL;

import no.uib.inf101.sem2.grid.CellPosition;

public class Player {
    private char color;
    private CellPosition pos;
    private CellPosition oldCp;
    private Direction dir;


    public Player (char color, CellPosition pos, Direction dir) {
        this.pos = pos;
        this.color = color;
        this.dir = dir;
    }

    /**
     * @return Player
     */
    public Player getPlayer(){
        return this;
    }

    /**
     * @return Player's CellPosition
     */
    public CellPosition getCellPosition() {
        return this.pos;
    }

    /**
     * @return players color in char
     */
    public char getCharColor() {
        return this.color;
    }

    /**
     * @return Player's current direction
     */
    public Direction getDirection() {
        return this.dir;
    }

    /**
     * @param newDir
     * @return Player's new direction
     */
    public Direction setDirection(Direction newDir) {
        this.dir = newDir;
        return this.dir;
    }

    /**
     * @param cp
     * @return new CellPosition
     */
    public CellPosition setPos(CellPosition cp) {
        this.pos = cp;
        return this.pos;
    }

    /**
     * 
     * @return Player's previous CellPosition
     */
    public CellPosition previousCP() {
        return this.oldCp;
    }

    private Player shiftedBy(int deltaRow, int deltaCol) {
        this.oldCp = new CellPosition(pos.row(), pos.col());
        int newrow = pos.row() + deltaRow;
        int newcol = pos.col() + deltaCol;
        CellPosition newcp = new CellPosition(newrow, newcol);
        this.pos = newcp;
        return this;
    }

    /**
     * Player takes one step
     * changes depending on the players direction
     * @param dir Direction
     * @return
     */
    public Player whereToStep(Direction dir) {
        Player p = this;
        if (dir == Direction.NORTH) {
            p = this.shiftedBy(-1, 0);
        } else if (dir == Direction.SOUTH) {
            p = this.shiftedBy(+1, 0);
        } else if (dir == Direction.WEST) {
            p = this.shiftedBy(0, -1);
        } else if (dir == Direction.EAST) {
            p = this.shiftedBy(0, +1);
        }
        return p;
    }


    
    

    
    
}
