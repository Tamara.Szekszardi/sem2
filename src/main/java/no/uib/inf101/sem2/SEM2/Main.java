package no.uib.inf101.sem2.SEM2;

import javax.swing.JFrame;

import no.uib.inf101.sem2.SEM2.CONTROLLER.Controller;
import no.uib.inf101.sem2.SEM2.MODEL.Board;
import no.uib.inf101.sem2.SEM2.MODEL.Model;
import no.uib.inf101.sem2.SEM2.VIEW.SemView;



public class Main {
  public static final String WINDOW_TITLE = "INF101 Sem2";

  public static void main(String[] args) {
    Board board = new Board(30,20);
    Model model = new Model(board);
    SemView view = new SemView(model);
    Controller kontroller = new Controller(model, view);

    // The JFrame is the "root" application window.
    // We here set som properties of the main window,
    // and tell it to display our tetrisView
    JFrame frame = new JFrame(WINDOW_TITLE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Here we set which component to view in our window
    frame.setContentPane(view);

    // Call these methods to actually display the window
    frame.pack();
    frame.setVisible(true);
    
  }

}
