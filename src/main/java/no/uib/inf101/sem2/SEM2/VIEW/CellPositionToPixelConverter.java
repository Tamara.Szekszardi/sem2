package no.uib.inf101.sem2.SEM2.VIEW;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;


public class CellPositionToPixelConverter {
    Rectangle2D box; //beskriver innenfor hvilket område rutenettet skal tegnes
    GridDimension gd; //beskriver størrelsen til rutenettet rutene vil være en del av
    double margin; //beskriver hvor stor avstanden skal være mellom rutene

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D getBoundsForCell(CellPosition cp) {
        Double cellWidth = (box.getWidth() - margin*(gd.cols()+1)) /gd.cols();
        Double cellX = box.getX() + cp.col()*cellWidth + (cp.col()+1)*margin; 
        Double cellHeight = (box.getHeight() - margin*(gd.rows()+1)) /gd.rows();
        Double cellY = box.getY() + cp.row()*cellHeight + (cp.row()+1)*margin;
        Rectangle2D cellBox = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
        return cellBox;
      }
}
