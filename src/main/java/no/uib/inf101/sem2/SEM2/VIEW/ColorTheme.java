package no.uib.inf101.sem2.SEM2.VIEW;
import java.awt.Color;



public interface ColorTheme {
    /**
     * Returnerer en farge (kan ikke være null), 
     * input:en bokstav => output: en farge
     * @param color
     * @return
     */
    Color getCellColor(char c);

    /**
     * Frame color
     * Returverdi bør ikke være null, 
     * gjennomsiktig farge: (new Color(0, 0, 0, 0))
     * @return Color
     */
    Color getFrameColor();

    /**
     * Bakgrunnsfarge
     * (kan være null men !ikke gjennomsiktig!)
     * @return
     */
    Color getBackgroundColor();
    
    /**
     * Game-Over farge
     * @return
     */
    Color gameOverBackground();
}
