package no.uib.inf101.sem2.SEM2.VIEW;

import java.awt.Color;


public class DefaultColorTheme implements ColorTheme {

    private static final Color FRAMECOLOR = new Color(0, 0, 0, 0);
    private static final Color BACKGROUNDCOLOR = null;
    private static final Color gameOverBackground = new Color(0, 0, 0, 128);

    @Override
    public Color getCellColor(char c) {
        Color color = switch (c) {
            case 'r' -> Color.RED;
            case 'o' -> Color.ORANGE;
            case 'y' -> Color.YELLOW;
            case 'g' -> Color.GREEN;
            case 'c' -> Color.CYAN;
            case 'b' -> Color.BLUE;
            case 'm' -> Color.MAGENTA;
            case 'p' -> Color.PINK;
            case 'w' -> Color.WHITE;
            case '-' -> Color.BLACK;
            default -> throw new IllegalArgumentException(
                    "No available color for '" + c + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return FRAMECOLOR;
    }

    @Override
    public Color getBackgroundColor() {
        return BACKGROUNDCOLOR;
    }

    @Override
    public Color gameOverBackground() {
        return gameOverBackground;
    }

}
