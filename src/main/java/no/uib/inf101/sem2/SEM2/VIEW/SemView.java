package no.uib.inf101.sem2.SEM2.VIEW;

import java.awt.Dimension;
import javax.swing.JPanel;

import no.uib.inf101.sem2.SEM2.MODEL.GameState;
import no.uib.inf101.sem2.SEM2.MODEL.Player;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.view.Inf101Graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Font;


public class SemView extends JPanel {
    private ViewableModel model;
    private ColorTheme farge;

    private static final Double OUTERMARGIN = 1.0;

    public SemView(ViewableModel model) {
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(300, 400));
        this.farge = new DefaultColorTheme();
        this.setBackground(farge.getBackgroundColor());
        this.model = model;

        
    }
    
    // The paintComponent method is called by the Java Swing framework every time
    // either the window opens or resizes, or we call .repaint() on this object.
    // Note: NEVER call paintComponent directly yourself
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGame(g2);
    }

    /**
     * tegner selve spillet
     * 
     * @param g2
     */
    private void drawGame(Graphics2D g2) {
        Player P1 = this.model.getPlayer1();
        Player P2 = this.model.getPlayer2();

        double height = this.getHeight();
        double width = this.getWidth();

        Rectangle2D rekt = new Rectangle2D.Double(0 + OUTERMARGIN, 0 + OUTERMARGIN, width - OUTERMARGIN,
                height - OUTERMARGIN);
        g2.setColor(farge.getFrameColor());
        g2.fill(rekt);
        CellPositionToPixelConverter pixel = new CellPositionToPixelConverter(rekt, model.getDimension(),
                OUTERMARGIN);
        
        drawCells(g2, model.getTilesOnBoard(), pixel, farge);
        drawPlayer(g2, P1);
        drawPlayer(g2, P2);

        if (model.getGameState() == GameState.GAME_OVER) {
            g2.setColor(farge.gameOverBackground());
            g2.fill(rekt);

            getScoreOnView(g2);

            String text = "GAME OVER";
            g2.setColor(farge.getCellColor('w')); // #
            g2.setFont(new Font("Arial", Font.BOLD, 48));
            Inf101Graphics.drawCenteredString(g2, text, rekt);

            Rectangle2D winnerPlayer = new Rectangle2D.Double(0 + OUTERMARGIN, 60 + OUTERMARGIN, width - OUTERMARGIN,
                height - OUTERMARGIN);
            char winnersColor = model.getWinner().getCharColor();
            g2.setColor(farge.getCellColor(winnersColor));
            g2.setFont(new Font("Arial", Font.BOLD, 32));
            Inf101Graphics.drawCenteredString(g2, model.getWinnerOnView(), winnerPlayer);
        }
        if (model.getGameState() == GameState.WELCOME) {
            g2.setColor(farge.gameOverBackground());
            g2.fill(rekt);

            String text = "WELCOME";
            g2.setColor(farge.getCellColor('w'));
            g2.setFont(new Font("Arial", Font.BOLD, 48));
            Inf101Graphics.drawCenteredString(g2, text, rekt);
            

            Rectangle2D instruction = new Rectangle2D.Double(0 + OUTERMARGIN, 50 + OUTERMARGIN, width - OUTERMARGIN,
                    height - OUTERMARGIN);
            String enter = "Please click ENTER to start";
            g2.setColor(farge.getCellColor('w'));
            g2.setFont(new Font("Arial", Font.BOLD, 20));
            Inf101Graphics.drawCenteredString(g2, enter, instruction);

        } 

    }

    private void getScoreOnView(Graphics2D g2) {
        Player P1 = this.model.getPlayer1();
        Player P2 = this.model.getPlayer2();

        double height = this.getHeight();
        double width = this.getWidth();

        Rectangle2D scoreTable1 = new Rectangle2D.Double(0 + OUTERMARGIN, 0 + OUTERMARGIN, width - OUTERMARGIN - 50,
            height - OUTERMARGIN - 160);
        g2.setColor(farge.getCellColor(P1.getCharColor()));
        g2.setFont(new Font("Arial", Font.BOLD, 60));
        Inf101Graphics.drawCenteredString(g2, String.valueOf(model.getscoreP1()), scoreTable1);

        Rectangle2D scoreTable2 = new Rectangle2D.Double(50 + OUTERMARGIN, 0 + OUTERMARGIN, width - OUTERMARGIN,
            height - OUTERMARGIN - 160);
        g2.setColor(farge.getCellColor(P2.getCharColor()));
        g2.setFont(new Font("Arial", Font.BOLD, 60));
        Inf101Graphics.drawCenteredString(g2, String.valueOf(model.getscoreP2()), scoreTable2);
    }

    private void drawPlayer(Graphics2D g2, Player player) {
        double height = this.getHeight();
        double width = this.getWidth();
        Rectangle2D rekt = new Rectangle2D.Double(0 + OUTERMARGIN, 0 + OUTERMARGIN, width - OUTERMARGIN,
                height - OUTERMARGIN);
        CellPositionToPixelConverter pixel = new CellPositionToPixelConverter(rekt, model.getDimension(),
                OUTERMARGIN);
        Rectangle2D player1 = pixel.getBoundsForCell(player.getCellPosition());
        g2.setColor(farge.getCellColor(player.getCharColor()));
        g2.fill(player1);
    }

    /**
     * tegner en samling med ruter
     * 
     * @param lerret
     * @param ruter
     * @param posTilRekt
     * @param farge
     */
    private static void drawCells(Graphics2D lerret, Iterable<GridCell<Character>> ruter,
            CellPositionToPixelConverter posTilRekt, ColorTheme farge) {
        for (GridCell<Character> cell : ruter) {
            Rectangle2D smallBox = posTilRekt.getBoundsForCell(cell.pos());
            lerret.setColor(farge.getCellColor(cell.value()));
            lerret.fill(smallBox);
        }
    }
}
