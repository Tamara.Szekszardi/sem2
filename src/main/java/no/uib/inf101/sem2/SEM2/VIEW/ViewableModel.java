package no.uib.inf101.sem2.SEM2.VIEW;

import no.uib.inf101.sem2.SEM2.MODEL.GameState;
import no.uib.inf101.sem2.SEM2.MODEL.Player;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public interface ViewableModel {
   
    /**
     * Gets the current value at the given coordinate.
     * @return GridDimension
     */
    GridDimension getDimension();
    
    /**
     * itererer over alle flisene på brettet. 
     * Mer presist, en metode som returnerer et objekt som, 
     * når det itereres over, gir alle posisjonene på 
     * brettet med tilhørende symbol.
     * @return Iterable<GridCell<Character>>
     */
    Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * 
     * @return Player 1
     */
    public Player getPlayer1();

    /**
     * 
     * @return Player 2
     */
    public Player getPlayer2();

    /**
     * 
     * @return Current gamestate
     */
    public GameState getGameState();


    /**
     * gets out scorevalue for Player 1
     * @return int
     */
    public int getscoreP1();

    /**
     * gets out scorevalue for Player 2
     * @return int
     */
    public int getscoreP2();

    /**
     * @return winner player
     */
    public Player getWinner();

    /**
     * @return name of winner player
     */
    public String getWinnerOnView();

    
}

