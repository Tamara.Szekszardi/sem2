package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E> {
    private int rows;
    private int cols;
    private ArrayList<ArrayList<E>> grid;
    private E element = null;

    public Grid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();


        for (int i = 0; i < rows; i++) {
            ArrayList<E> row = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                row.add(j, element);
            }
            this.grid.add(row);
        }
    }

    public Grid(int i, int j, E defaultValue) {
        this.rows = i;
        this.cols = j;
        this.element = defaultValue;
        this.grid = new ArrayList<>();

        for (int r = 0; r < rows; r++) {
            ArrayList<E> row = new ArrayList<>();
            for (int c = 0; c < cols; c++) {
                row.add(c, element);
            }
            this.grid.add(row);
        }
    }

    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        ArrayList<GridCell<E>> allCell = new ArrayList<GridCell<E>>();
        for (int i = 0; i<rows; i++) {
            for (int x = 0; x<cols; x++) {
                CellPosition cellPosition = new CellPosition(i, x);
                E element = get(cellPosition);
                GridCell<E> gridCell = new GridCell<E>(cellPosition, element);
                allCell.add(gridCell);
            }
          } 
        return allCell.iterator();
    }

    @Override
    public void set(CellPosition pos, E value) {
        int row = pos.row();
        int col = pos.col();

        ArrayList<E> listE = grid.get(row);
        listE.set(col, value);

        grid.set(row, listE);
    }

    @Override
    public E get(CellPosition pos) {
        // plukker ut posisjonen fra liste
        int r = pos.row();
        int c = pos.col();
        ArrayList<E> row = grid.get(r);
        // finner og returnerer funnet farge
        E cell = row.get(c);
        return cell;
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        //finne gyldig posisjon
        if (pos.row() < 0) return false;
        if (pos.row() >= this.rows()) return false;
        if (pos.col() < 0) return false;
        if (pos.col() >= this.cols()) return false;
        else return true;
    }

}
