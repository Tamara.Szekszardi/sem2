package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.SEM2.MODEL.Board;
import no.uib.inf101.sem2.grid.CellPosition;

public class TestBoard {
    @Test
    public void TestStringBoard() {
        Board board = new Board(3, 5);
        board.set(new CellPosition(0, 2), 'r');
        board.set(new CellPosition(0, 4), 'y');
        board.set(new CellPosition(1, 2), 'y');
        board.set(new CellPosition(1, 3), 'b');
        board.set(new CellPosition(2, 0), 'r');
        board.set(new CellPosition(2, 2), 'g');
        board.set(new CellPosition(2, 4), 'b');
        String expected = String.join("\n", new String[] {
                "--r-y",
                "--yb-",
                "r-g-b"
        });
        assertEquals(expected, board.stringBoard());
    }

    @Test
    public void TestRemoveAll() {
        // Tester at fulle rader fjernes i brettet:
  // -T
  // TT
  // LT
  // L-
  // LL

  Board board = new Board(5, 2);
  board.set(new CellPosition(0, 1), 'b');
  board.set(new CellPosition(1, 0), 'b');
  board.set(new CellPosition(1, 1), 'b');
  board.set(new CellPosition(2, 1), 'b');

  board.set(new CellPosition(4, 0), 'r');
  board.set(new CellPosition(4, 1), 'r');
  board.set(new CellPosition(3, 0), 'r');
  board.set(new CellPosition(2, 0), 'r');

  board.removeAll();

  String expected = String.join("\n", new String[] {
    "--",
    "--",
    "--",
    "--",
    "--"
  });
  assertEquals(expected, board.stringBoard());
    }
}
