package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.SEM2.MODEL.Board;
import no.uib.inf101.sem2.SEM2.MODEL.Direction;
import no.uib.inf101.sem2.SEM2.MODEL.Model;
import no.uib.inf101.sem2.SEM2.MODEL.Player;
import no.uib.inf101.sem2.grid.CellPosition;

public class TestModel {
    @Test
    public void modelSpawnedPlayer() {
        Board board = new Board(7, 2);
        Model model = new Model(board);
        CellPosition p1cp = model.getPlayer1().getCellPosition();
        CellPosition newCP = new CellPosition(p1cp.row() + 3, p1cp.col());
        
        assertEquals(p1cp.col(), newCP.col());
    }

    @Test
    public void testClockTick() {
        Board board = new Board(7, 2);
        Model model = new Model(board);
        CellPosition p1cp = model.getPlayer1().setPos(new CellPosition(5, 1));

        model.clockTick();

        CellPosition newcp = model.getPlayer1().getCellPosition();

        assertEquals(p1cp.col(), newcp.col());
        assertEquals(p1cp.row() -1, newcp.row());
    }


    @Test
    public void testClockTick2() {
        Board board = new Board(9, 5);
        Model model = new Model(board);
        CellPosition p1cp = model.getPlayer1().setPos(new CellPosition(3, 3));
        model.getPlayer1().setDirection(Direction.EAST);

        model.clockTick();
        model.clockTick();

        model.getPlayer1().setDirection(Direction.SOUTH);

        model.clockTick();

        CellPosition newcp = model.getPlayer1().getCellPosition();

        assertEquals(p1cp.col() +2, newcp.col());
        assertEquals(p1cp.row() +1, newcp.row());
    }

    @Test
    public void TestGetWinnerOnView() {
        Board board = new Board(4, 5);
        Model model = new Model(board);
        Player player1 = model.getPlayer1();
        Player player2 = model.getPlayer2();

        player1.setPos(new CellPosition(3, 2));
        player1.setDirection(Direction.EAST);
        player2.setPos(new CellPosition(0, 0));
        player2.setDirection(Direction.NORTH);

        model.clockTick();
        model.clockTick();

        model.getWinnerOnView();

        String string = "PLAYER 1 WON";

        assertEquals(string, model.getWinnerOnView());
    }

    @Test
    public void playerCrashLine() {
        Board board = new Board(4, 5);
        Model model = new Model(board);
        Player player1 = model.getPlayer1();
        Player player2 = model.getPlayer2();

        player1.setPos(new CellPosition(3, 4));
        player1.setDirection(Direction.EAST);
        player2.setPos(new CellPosition(3, 3));
        player2.setDirection(Direction.NORTH);

        model.clockTick();
        model.clockTick();


        String string = "PLAYER 2 WON";
        assertEquals(string, model.getWinnerOnView());
    }

    @Test
    public void playerCrashWall() {
        Board board = new Board(20, 20);
        Model model = new Model(board);
        Player player1 = model.getPlayer1();
        Player player2 = model.getPlayer2();

        player1.setPos(new CellPosition(10, 18));
        player1.setDirection(Direction.EAST);
        player2.setPos(new CellPosition(10, 1));
        player2.setDirection(Direction.NORTH);

        model.clockTick();
        model.clockTick();

        model.clockTick();

        String string = "PLAYER 2 WON";
        assertEquals(string, model.getWinnerOnView());
    }

    @Test
    public void playerCrashPlayer() {
        Board board = new Board(20, 20);
        Model model = new Model(board);
        Player player1 = model.getPlayer1();
        Player player2 = model.getPlayer2();

        player1.setPos(new CellPosition(10, 10));
        player1.setDirection(Direction.EAST);
        player2.setPos(new CellPosition(10, 12));
        player2.setDirection(Direction.WEST);

        model.clockTick();
        model.clockTick();

        model.clockTick();

        String string = "LOOSERS";
        assertEquals(string, model.getWinnerOnView());
    }
}