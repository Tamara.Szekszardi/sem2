package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.SEM2.MODEL.Board;
import no.uib.inf101.sem2.SEM2.MODEL.Direction;
import no.uib.inf101.sem2.SEM2.MODEL.Model;
import no.uib.inf101.sem2.SEM2.MODEL.Player;
import no.uib.inf101.sem2.grid.CellPosition;

public class TestPlayer {
    @Test
    public void TestWhereToStep() {
        Board board = new Board(9, 5);
        Model model = new Model(board);

        Player P1 = model.getPlayer1();

        CellPosition p1cp = P1.setPos(new CellPosition(3, 3));
        Direction dir1 = P1.setDirection(Direction.SOUTH);

        P1.whereToStep(dir1);
        P1.whereToStep(dir1);

        CellPosition newcp = P1.getCellPosition();

        assertEquals(p1cp.col(), newcp.col());
        assertEquals(p1cp.row() +2, newcp.row());
    }

    @Test
    public void TestWhereToStep2() {
        Board board = new Board(9, 5);
        Model model = new Model(board);

        Player P2 = model.getPlayer2();

        CellPosition p1cp = P2.setPos(new CellPosition(3, 3));
        Direction dir2 = P2.setDirection(Direction.SOUTH);
        P2.whereToStep(dir2);
        P2.whereToStep(dir2);

        dir2 = P2.setDirection(Direction.WEST);
        P2.whereToStep(dir2);

        CellPosition newcp = P2.getCellPosition();

        assertEquals(p1cp.col() -1, newcp.col());
        assertEquals(p1cp.row() +2, newcp.row());
    }

}
