package no.uib.inf101.sem2.view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.SEM2.MODEL.Board;
import no.uib.inf101.sem2.SEM2.VIEW.CellPositionToPixelConverter;


import java.awt.geom.Rectangle2D;

public class TestCellPositionToPixelConverter {
    @Test
    public void sanityTest() {
        GridDimension gd = new Board(3, 4);
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(
                new Rectangle2D.Double(29, 29, 340, 240), gd, 30);
        Rectangle2D expected = new Rectangle2D.Double(214, 129, 47.5, 40);
        assertEquals(expected, converter.getBoundsForCell(new CellPosition(1, 2)));
    }

}
